<?php
/*
Define whether or not the IP limitation functions are enabled.
  true = enabled
  false = disabled
Only set false for testing.
*/
define('RESTRICT_IPS', TRUE);
/*
Define what IP addresses are allowed to update the messages.
  separate each IP address with a single space
*/
define('ALLOWED_IPS', '129.97.128.133 129.97.85.110');
/*
Define the maximum time difference (in seconds) allowed between the timestamp in the request and the server timestamp.
  600 = 10 minutes plus or minus
*/
define('ALLOWED_OFFSET', 600);
