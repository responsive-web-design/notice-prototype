<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>University of Waterloo | Alert</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <style>
    html {
      font-family: sans-serif;
      -ms-text-size-adjust: 100%;
      -webkit-text-size-adjust: 100%;
      font-size: 62.5%;
    }

    body {
      margin: 0;
      font-size: 1.5em;
      line-height: 1.6;
      font-weight: 400;
      font-family: Arial, Helvetica, sans-serif;
    }

    img {
      border: 0;
    }

    .container {
      position: relative;
      width: 100%;
      margin: 0 auto;
      box-sizing: border-box;
    }

    h1 {
      margin-top: 0;
      margin-bottom: 2rem;
      font-weight: 300;
      font-size: 4rem;
      line-height: 1.3;
      letter-spacing: 0.04rem;
    }

    p {
      margin-top: 0;
      font-size: 2.5rem;
      line-height: 1.3;
    }

    a {
      color: #000;
      background-color: transparent;
    }

    a:active,
    a:hover {
      outline: 0;
    }

    .bar {
      background: #000;
      height: 7px;
      width: 100%;
      position: absolute;
      z-index: 2;
    }

    #logo {
      padding-bottom: 47px;
    }

    #logo img {
      width: 100%;
      max-width: 234px;
    }

    .container {
      max-width: 800px;
      padding: 35px 45px 36px 45px;
      border: 1px solid #959595;
      text-align: center;
      -webkit-box-shadow: 4px 6px 5px -3px rgba(0,0,0,0.36);
      -moz-box-shadow: 4px 6px 5px -3px rgba(0,0,0,0.36);
      box-shadow: 4px 6px 5px -3px rgba(0,0,0,0.36);
    }

    #alert {
      display: flex;
    }

    #main {
      flex-grow: 1;
    }

    .posttime {
      font-size: 100%;
    }

    #sidebar {
      width: 25%;
      border-left: 1px solid #000000;
      padding-left: 45px;
      margin-left: 45px;
    }

    #sidebar h2 {
      margin: 0 0 20px 0;
    }

    #sidebar ul {
      margin: 0;
      padding: 0;
      list-style: none;
    }

    #sidebar li {
      padding: 0 0 10px;
    }

    @media (max-width:825px) {
      #alert {
        flex-wrap: wrap;
      }

      #sidebar {
        width: 100%;
        border-left: none;
        border-top: 1px solid #000000;
        padding: 45px 0 0 0;
        margin: 45px 0 0 0;
      }
    }


    @media (max-width:525px) {
      #logo {
        padding-bottom: 25px;
      }

      .container {
        border: none;
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        box-shadow: none;
      }

      h1 {
        font-size: 3rem;
      }

      #logo img {
        max-width: 200px;
      }
    }
    @media (max-width:443px) {
      .container{
        padding: 35px 15px 36px 15px;
      }
    }
  </style>
</head>
<body>
  <div class="bar"></div>
  <div class="container">
    <div id="logo">
      <img alt="University of Waterloo" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOoAAACZCAMAAAAW/FYTAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAA8UExURf7aUAEBAaenp1FSUu+sSf///8vMzLUMM+Dg4M9UPe/v79+AQ2BRHLecOcEtOJB5K/j4+Nm5RCYlIoGBge0S/EcAAA8bSURBVHja7V2LYquqElVAFEwsgf//1wvDawYxfZyanfSG3XOqosBi3gPaYSLFMM5lOJCcu3hJc85NOPBVLN3m0l3TxPMRKw/4o1QWesHp0o921lrOyoXcB/SWb89dNQe0MA4tlXPX9l7K0JzzcWShUzaOPF5ZxnF04ZLzv+td0uQjng9Srb8vlXRPuWDT/EyLpReMP9elt1RiuzK1A793QDXPNy91aKl8BtUPC2YIQQ3DYqgqQS0TYXUaoq5QLRRJLoT/R+zGX7HcN2LTcLSNrcSjOAto5sJjtk40YkJolMP/4/OG587tN6C6Cc2zPKBqZgNXpiY8ygyUesFNxjBbJsiGOTey8F2l6qS1DtPsf+la50Ibds+9YRJ974ZlzotD07j3I6jmgKphYrtUNenGCgPfVy64PCsFKhJcgGPruazTlk6ttF32tblFlpnK8N6NX2XgwBHhrE/V0N1iJJr0yJ1FX8U2TbxTVoHyumTBjGiOoCbxczsyTahXm0cUdAaU5ROouktVG3QC61IV+MAZTEmW9YKrF6zv2xZZBdQBrZtaWQ0ttlA1kkWqfeuNmWNMUUts+omsWhgt71IVphZkzSCqQmHTTiUjo4bHc5+qMHls6kK1DdTpy1APqGqMI49jgQjz6LB6dFktTQ1UrBPNpFkxVFRWd1SFatOBupTZW7JxCaNhMpTP1BJMlCEqBKiaWK5H1cSw5Jy1bbqF2MUFJGkpREEauEdVwt+NFEOFrrP2dbUUpscyx6sJXuKULkdUTaJEZIATzRBp7qq8hSccQ1S9L6vHUCUIPAtqIA3oGxq4Cpar4G3yn/pUjdat00K+mNjbNvYLCe9eVs2XoFYV2KfCfagTi3a0jH3JguKO2pMjZr9990nFyWIFjXa0k7t29R5U76tHv1BOP4A6ackYFupl0c3BpJcFeyOLxEbMV/o7ww+6Oz2PuNQrD9wHVlm1o1ptpoOyMMYW0rv5MtRHFfPg/v4h1OkN9Q31DfUN9Y9BNd+rH2LKiaHLMRFFMlU0CRbPkdPrumWB8KFTkuMgyTViHEujB96Ali5kzyx3rOtamF79EL0NFDiYJt7KISAnCR3s9dbMFSksOVK7Yg8eszgzYYl3Sp0GZ9FT+xhc43pbEmwDG/eBCe0kJfGYaRw/+R+huk4N+zz1kDxXDPaz+tjIoNvb+dgQjTW5Rk6Slz+GatzRQyjr6bqBW1u4phH3rn6JaolTZNqOXWgcO/ex6F+nKuKVAwY23c5QMHBcP2SiyaPMUMLO9m25H0Lld6iKMyg9qLUvG1PLlMNQo7TeBKj6YNycYC/JEl2fz7pMhijF/7gkb+lUV6gp1oH/ShDjUvgVS5EweQ9qJURo3kjeTHupBwNhpEPyOjQMipAseJ74PvDG8XhoN09KOjWI23sWQZOGTWnZ3WFgXRZAShKPyJtpF0jMUuuHRu2wVrEZyr/p1BW22E05CaIT1K6xb+awNH2Hqm4nmgW9M3X0OF+X63mASoSR95aHUOMsqTzbGN/7UHtUbdmFspfpQrXjvltZlaTp1S95egbaRXYPKp1dz7CwHUnuQrVfoiq90GNgOXZ6zaT2PH23ng10iIlorvZD+TcP3fTIdUzV4PeVYnqy+iUGdh2iTXkobmqNSU7fpPqBimNsTMoyioUOv85BZyjHUEnRB1R1RNcdQ215JA2fl3ptemzPB9KsTqtupnAwHY+uGoztu/0e1Cyrxmj/byl2Qx8ysGm8nZ2M3623AxmjpPPjeY0srWT+7mjmr0Mtk+6qqUfG3pljqvKeKJbLph4cQzVZn/L022TMC6GdwXp3r5i+R1Xt7rl4PQ3836gaGbhMYV41zqA0I3iSUgLlIvc64key2iBd7gVxfN8BkkWTWzyoHzAGWZrPK2+OLDf0nF33FahykeVn2burvRClx8BdDVu0fNwnsF9uxBq4qCNbQ9cE3uLBL13/XP83b6kX1RxBlWNPGCt7fW5XqZOUBNO2btNRKOJ+6C0lWQ3r6Y7vY+weA2dviHXww7B79dlbMsNOpna7j/Jka3s36vqxD2wmo/ce3V0fGK/2WDRstq+vPvJA56t2J1syGzYex6U/gEp8YDY2ZrrvA5fIpvSZIxvo1ezqm8iGEtE2rnW1dImlZC2Nnbunlhb8Y/bWio9NLimpTexRLjgeZdr7HjVeTe7mcX3JA2u5Ez1Hydzzepus0xeNTbqD+sA7FuZ9rYCVyl6dEH3S1g+NxNeU0tIY9V4Kz5JrX4Wapst1kh31ad6XlH5uiX+Se+IaQ3WtkjFULS49m0Zt9veoarrhYTm3R7q+R26zUwEUqSELGXKnp11n+6ftWmciKD+kamFhdpeqnTyvZWQ/odnXN2s2xrY7S4n/q/v7nsjawDehts47zZbYYwtOs/dO73U7qV92y1MStBzxw8DXTc2TsJpsRPBF4xN8j2a9gjuUTX/5itw/tpA1GW79v6NFnVpvnn4l7oSNEu/11TfUN9Q31DfUN9Q31DfUN9QHQtUh+/lL//RzQ3Xj7xX37FA/fqs8P9Tb8Dvl9n8AVawiQmXPDZUB1NUPuAx9VUKsnyP0t8Xf8zyrl6HqOisxp4Gr2f/MW6WZ8kUgGop4dVvXeVObEHMo60tQ9epHvs3bvEYIq1LbPBdsCpB4UCLRfZvhsr9vA4hrhnp9CahrGi2wMZBJKIIUir8ktjANYvXzAVUbcIR6FareAi0BqgiEWodArcTPK0XqoQPFPeuuAHX1xcPdAr+/BlUB7BZ06ep/AtI1IvPAAh29tEYt68ma5DjMThBXJfwpcMSzQzUyQg1sq5IWWtdwtkWiqpufAFWU0hbYdRNBfSkvp/7Us7oA8X16qmaoAKvYkSiGnkfjpWp6/TQEzlWZsP7II1YRqnwJBo74NlBN6zYXqKLnMsRalVXvoMAAvQhVQRK3OHRVtZDaBupMeFsUyanEFm8LDwaWf36qmmX88MME8QMJzRAiBu9ZzMRDSr7FWm6Dx+Dpj2dnYBmgZjn13mw0L1vC7tWvIDTN59nebnGGXgJqoCo4DSo7QxVD1UuxVPMaD/1EKAx1eX6oQ2RH79EmEfREDVT1VgY5w2BgEmf7Kk93EcOCV4GqxwvIqgcFEhpV0yqCUlVJGCsHb0BJUGICnIgk4itA1a8A1QPz4/denvAeUPBxI4ZhJQwMpymSWbc6ETFUuDw7VGMvjdnckEgOG7U1a1ZJ61r1dHSBh4t9cqhTC1UgY+NRdH2I4A+Wu8Qa5+MFoFqMwltSiG+SCl63wwxEiVO3xALWTq8BNaZPYjAXYzdPUNElaoa7xQnZVJyQ54fKbTKUKppLTy8V6ZozE0G73vp8DMFrYnPLnx7qmO1kTLzUxEPJKN3saHdY1/UmMu+CDzw+PdSURwNoSN/MNb90hU1FH1QNQ0QrcmijzkgYngNVbKqQMvl8xcx8pGWKj5JYUymkFTE3M0DA+gJQU8CqgHVDOKaQWQXPIJfLLQfkMcZJSjh4FeKMLNpZUANbipQK3rY5K6Qb3k5nr0lJq/ArexFzZIDrKM2TQ12qFCLzmuh6zfsbeWFiSLNAjhQOkh98Rgx3NlRw5dc5R9t5c27eyXqBSHyDvD5KV5wS2Pz+qvl4oWkG78knSl3qsqnJLwJ4gRVJCWcGjrx+sebpoWLPsGiaKqblBYL0foG94vgVljNO8gt/H2r0IfC6BRHTKoELp1anPnOOB/H7UPEKK4TbM7Km5L0nJLDlAeBjcYpZ/X2oNRMcs/dh+LdLd3dDFtjiJipYijtnxeYEqBJzJHizOzGdinJtBNab4JSDOMHW/D5UPbZ5iGv7vqarr1BoLLApGzyckm45Y4sWDc6LjanfMOL409sTEtgYxZ8UrZ4BFavgKqb4Y5dUFWOB9dwbV51PUMAnQCWbXLKYyvwqTH0Jhpm9wCa3+RQFfAJUrJeyjVkMZVf6zpPeWdgztNIJUJFeyjYmfzMgY7o038qfWgt7hlY6Y+do1ktFTPP3B/K7pNeUicACa4mFPUNUz4CahPW6E1PsMNjGqZBlGqKovsYrRVFYPz5h0kvjKhoksKeI6hlQQVgvn6qe64HKgofNa0D1lvXWvspP+LMxRKwV2OsponoKVNb+oRMqpt7VDZGp2EcB5R149ipQdfPpDhqtrRvssFQDScEUhjj8jsRTQo0DPhDTsHYOGXCBsFqqvE7h33Ogso6YxkAtrBlDAKPinrO9wNbvuL8EVG1rvg+LqRDCIw2LFmH7jqAuVeb2ZbT6daB6Ds5/fqiK6apucV/SFrcTznnjcyOw+iT+PQmqTETCqTLYOimAtDkuVXFzXond018ykK8E1fvB8B1M7NnG7ZK3utMblt9gRwg2w/qMqPxMqCw4+QyvQw1pQVmsZe8zLL+JjQosO0kpnQbV06ZEbEBRERbmYNfhXLP4sMgYU23FZbbWvBZUL3HYFYSVRQEZxLQLBKDC2nHa158F9oz8w7lQNRbTsG6zlqx22NiR95KGPLGiLrF5NajR7YlimjbqiFvckrau6T2GQNa1QE0CexpRz4MavglzKZnsOW9y2db4Msq8wS7RDWrzJp+PM4l64qu6DOW+N3gpQ21ZLW3pxTcRXeK1ps/Y9IJQce47atmwqW6OW/nTPqzhts74NbIziXomVLlb0oiuoKqbe0TdlBeFVU4vCdV7wtf9/jrPuXEvAGjgTeUXN8De8OlFoep2+Sa9shm3QYu8nlo2HtpzQvJHQDVYM0VxjW+kwLbDREu1VvZl06tCJSwcsQFVV7IbengM+54N1ftMt0zUuM8leIQKbWUpgno7KyJ/EFSvhS3a7B1diW0t+w7VpqqgyumloWZxzcmz8E5K9AtDvLqu4lGCOj3gwyYu50RDQkklFxDCG0WW1p15eaheNUW6qgF9V+CmCNaPs1XSY6B6rB+7Tz+gF5KBpg9A+pAv8/DWQwTDo+aaHuXmj0BtsIaNhCob2ofR9FHfW3LoxQQB6ZYINyYf3PSHoIbk4bVKqoeZldLVjsz8JagmpIQ/sKgm1/eDfGL8T1AVEv2JiUVJsHjm5Yv5c1Bhnyi1Oh/jo8T0wVAnEwhbg3UvpVxOfxPqZCZn87LG7TLaR5J0evjH/cIipAcbdkG4ZfrTUE3g4rDGweWj/wD2v/hko+T2sUL676B6Nv4Xnb4/xPkny/8AD1quMd+tS+YAAAAASUVORK5CYII=" />
    </div>
    <div id="alert">
      <div id="main">
        <h1>Alert</h1>
        <?php
        $message = file_get_contents('messages/message.inc.html');
        // If the file failed to load or is empty, it will fail this test.
        if (!$message) {
          $message = '<p>There are no notices at this time.</p>';
        }
        // Output the message with each line as a paragraph.
        echo $message;
        ?>
      </div>
      <div id="sidebar">
        <?php
        $sidebar = file_get_contents('messages/sidebar.inc.html');
        // If the file failed to load or is empty, it will fail this test.
        if (!$sidebar) {
          $sidebar = '<h2>Important links</h2>
                      <ul>
                        <li><a href="https://uwaterloo.ca">University of Waterloo home page</a></li>
                        <li><a href="https://twitter.com/uwaterloo">University of Waterloo Twitter feed</a></li>
                      </ul>';
        }
        // Output the sidebar.
        echo $sidebar;
        ?>
      </div>
  </div>
</body>
</html>