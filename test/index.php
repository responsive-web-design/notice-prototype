<?php
// Test updating the emergency page

/*
 *  Send queries to this file to update messages being sent.
 *  If any required values are missing or invalid, no changes are made.
 *
 *  Values:
 *  text (optional)
 *      If left blank, default message for type and location will be used.
 *      Plain text message - no HTML allowed. Output as a paragraph
 *      (or multiple paragraphs, if there are line breaks).
 *  type (required)
 *      Plain text alert type (all lower case).
 *      One of: ok, test, chemical, tornado, violent
 *  location (required, except for "ok" and "test" types)
 *      Plain text alert location (all lower case).
 *      One of: cambridge, kitchener, stratford, waterloo
 *      Note: "chemical" only works with kitchener or waterloo;
 *      "tornado" only works with stratford or waterloo
 */

error_reporting(E_ALL);
ini_set('display_errors', 1);

// URLs that make changes
$update_urls = array(
  'http://alert1.private.uwaterloo.ca/change_vNyx9TdXETL153lKeqwj.php',
  'http://alert2.private.uwaterloo.ca/change_vNyx9TdXETL153lKeqwj.php',
);

// Get data
$text = isset($_GET['text']) ? $_GET['text'] : '';
$type = isset($_GET['type']) ? $_GET['type'] : 'ok';
$location = isset($_GET['location']) ? $_GET['location'] : '';
$timestamp = time();

// Prepare to send
$data = array(
  'text' => $text,
  'type' => $type,
  'location' => $location,
  'timestamp' => $timestamp,
);

// Update all URLs
foreach($update_urls as $update_url) {
  // Create a connection
  $ch = curl_init($update_url);
  
  // Form data string
  $postString = http_build_query($data, '', '&');
  
  // Set options
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  
  // Get the response
  $response = curl_exec($ch);
  curl_close($ch);
  
  echo $response;
}
