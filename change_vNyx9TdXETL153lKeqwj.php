<?php
/*
 *  Post to this file to update messages being sent.
 *  If any required values are missing or invalid, no changes are made.
 *
 *  Values:
 *  text (optional)
 *      If left blank, default message for type and location will be used.
 *      Plain text message - no HTML allowed. Output as a paragraph
 *      (or multiple paragraphs, if there are line breaks).
 *  type (required)
 *      Plain text alert type (all lower case).
 *      One of: ok, test, chemical, tornado, violent
 *  location (required, except for "ok" and "test" types)
 *      Plain text alert location (all lower case).
 *      One of: cambridge, kitchener, stratford, waterloo
 *      Note: "chemical" only works with kitchener or waterloo;
 *      "tornado" only works with stratford or waterloo
 *  timestamp (required)
 *      UNIX timestamp indicating when message was sent.
 *      If this differs by more than the value of ALLOWED_OFFSET,
 *      no changes are made.
 */

// Get configuration.
require_once('config.inc');

// Assume this will fail.
$success = false;
error_reporting(E_ALL);
ini_set('display_errors', 1);

// Don't do anything if IP filtering is on and an invalid IP address was detected.
$allowed_ips = explode(' ', ALLOWED_IPS);
if (!RESTRICT_IPS || (RESTRICT_IPS && in_array($_SERVER['REMOTE_ADDR'], $allowed_ips))) {
  // Don't do anything if the post doesn't exist.
  $message = isset($_POST['text']) ? $_POST['text'] : false;
  $type = isset($_POST['type']) ? $_POST['type'] : false;
  $location = isset($_POST['location']) ? $_POST['location'] : false;
  $timestamp = isset($_POST['timestamp']) ? $_POST['timestamp'] : false;
  if ($timestamp && $type) {
    // If there's not a number where we're expecting a timestamp, don't go any further.
    // If the timestamp isn't within the allowed offset, don't go any further.
    if (is_numeric($timestamp) && abs(time() - $timestamp) <= ALLOWED_OFFSET) {
      // Folder containing the messages, with a trailing slash.
      $folder_base = 'messages/';
      // Folder that should contain the requested type, with a trailing slash.
      $message_path = $type;
      if ($type != 'ok' && $type != 'test') {
        // Get location-specifc version of messages.
        $message_path .= '-' . $location;
      }
      $folder = 'message_store/' . $message_path . '/';
      // If the folder of the requested type doesn't exist, or it doesn't contain the expected files, don't go any further.
      if (is_dir($folder) && is_file($folder . 'message.txt') && is_file($folder . 'sidebar.inc.html')) {
        // If the text variable is empty, use the stock text.
        if (!$message) {
          $message = file_get_contents($folder . 'message.txt');
        }
        // Clean up the message for output.
        // Make message HTML-safe
        $message = htmlspecialchars($message);
        // Split the message at each new line
        $message = preg_split("/\r\n|\n|\r/", $message);
        // Remove empty lines
        foreach ($message as $key => $this_message) {
          if ($this_message == '') {
            unset($message[$key]);
          }
        }
        // Make each line a paragraph.
        $message = '<p>' . implode('</p><p>', $message). '</p>';
        // Add the post time.
        $message = '<p class="posttime">Posted ' . date('F j, Y') . ' at ' . date('g:i a') . '.</p>' . $message;
        //Update files, overwriting whatever was there.
        if (file_put_contents($folder_base . 'message.inc.html', $message) && copy($folder . 'sidebar.inc.html', $folder_base . 'sidebar.inc.html')) {
          // We seem to have succeeded.
          $success = true;
        }
      }
    }
  }
}

// Report on success
if ($success) {
  echo 'Action succeeded.';
} else {
  echo 'Action failed.';
}